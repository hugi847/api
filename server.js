//version inicial

var express = require('express'),
  bodyParser = require('body-parser'),
  path = require('path'),
  https = require('https'),
  app = express(),
  querystring = require('querystring'),
  // requestJson = require('request-json'),
  port = process.env.PORT || 3000,
  mongoDB = 'bdbanca3mb79572',
  cMovimientos = 'movimientos',
  mLabApiKey = 'RMBD14WGGZ5Pt50BLxAt_U2LvSyRgKWT',
  mlab = 'api.mlab.com',
  mLabMovimientosURI = `/api/1/databases/${mongoDB}/collections/${cMovimientos}?apiKey=${mLabApiKey}`,
  mLabMovimientosHttpOpts = {
    hostname: mlab,
    port: 443,
    path: `${mLabMovimientosURI}`,
    agent: false,
    headers: {
      'Content-Type': 'application/json'
    }
  };

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use((req, res, done) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  done();
});
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// app.get('/', (req, res) => res.send('Hola mundo Node JS!!!'));
app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'index.html')));

app.get('/clientes', (req, res) => {
  // res.sendFile(path.join(__dirname, 'datos', 'clientes.json'));
  https
    .get(
      Object.assign(mLabMovimientosHttpOpts, {
        path: `${mLabMovimientosURI}&${querystring.stringify({
          f: '{ "_id": 0, "idCliente": 1, "nombre": 1, "apellido": 1 }'
        })}`
      }),
      apiResponse => {
        apiResponse.on('data', data => {
          res.send(data.toString());
        });
      }
    )
    .on('error', error => res.json({ mensaje: 'error en peticion JSON' }));
});

app.post('/clientes', (req, res) => {
  body = JSON.stringify(req.body);
  console.log('Body:', body);
  // res.json({ mensaje: 'Su peticion POST ha sido recibida', datos: req.body });
  var req = https
    .request(
      Object.assign(mLabMovimientosHttpOpts, {
        method: 'POST',
        'Content-Length': Buffer.byteLength(body)
      }),
      apiResponse => {
        console.log('statusCode: ', apiResponse.statusCode);
        apiResponse.on('data', data => {
          res.header('Content-Type', 'application/json');
          res.send(data.toString());
        });
        apiResponse.on('end', () => {
          res.end();
        });
      }
    )
    .on('error', error => res.json({ mensaje: 'error en peticion JSON' }));
  req.end(body);
});

app.get('/clientes/:idCliente', (req, res) => {
  console.log('idCliente', idCliente);
  res.sendFile(path.join(__dirname, 'datos', 'clientes.json'));
});

app.put('/clientes', (req, res) => {
  res.json({ mensaje: 'Su peticion PUT ha sido recibida', data: req.body });
});

app.delete('/clientes', (req, res) => {
  res.json({ mensaje: 'Su peticion DELETE ha sido recibida', data: req.body });
});

app.get('/movimientos', (req, res) => {
  // res.sendFile(path.join(__dirname, 'datos', 'clientes.json'));

  https
    .get(httpOpts, apiResponse => {
      apiResponse.on('data', data => {
        res.send(data.toString());
      });
    })
    .on('error', error => res.json({ mensaje: 'error en penticion JSON' }));
});
